//
//  NotesFlowLayout.swift
//  Work
//
//  Created by Paul Tang on 2019-04-13.
//  Copyright © 2019 Paul Tang. All rights reserved.
//

import UIKit

class NotesFlowLayout: UICollectionViewFlowLayout {
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        
        // Page height used for estimating and calculating paging.
        let pageHeight = self.itemSize.height + self.minimumLineSpacing
        
        // Make an estimation of the current page position.
        let approximatePage = self.collectionView!.contentOffset.y/pageHeight
        
        // Determine the current page based on velocity.
        let currentPage = (velocity.y < 0.0) ? floor(approximatePage) : ceil(approximatePage)
        
        // Create custom flickVelocity.
        let flickVelocity = velocity.y * 0.3
        
        // Check how many pages the user flicked, if <= 1 then flickedPages should return 0.
        let flickedPages = (abs(round(flickVelocity)) <= 1) ? 0 : round(flickVelocity)
        
        let newVerticalOffset = ((currentPage + flickedPages) * pageHeight) - self.collectionView!.contentInset.top
        
        return CGPoint(x: proposedContentOffset.x, y: newVerticalOffset)
    }
}
