//
//  SecondViewController.swift
//  Work
//
//  Created by Paul Tang on 2019-04-02.
//  Copyright © 2019 Paul Tang. All rights reserved.
//

import UIKit
import Pastel

class SecondViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let pastelView = PastelView(frame: view.bounds)
        
        // Custom Direction
        pastelView.startPastelPoint = .bottomLeft
        pastelView.endPastelPoint = .topRight
        
        // Custom Duration
        pastelView.animationDuration = 3.0
        
        // Custom Color
        pastelView.setColors([
            UIColor(red: 32/255, green: 76/255, blue: 255/255, alpha: 0.85)
            ])
        
        pastelView.startAnimation()
        view.insertSubview(pastelView, at: 0)
        
    }


}

