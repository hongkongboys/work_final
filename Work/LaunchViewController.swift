//
//  LaunchViewController.swift
//  Work
//
//  Created by Paul Tang on 2019-04-06.
//  Copyright © 2019 Paul Tang. All rights reserved.
//

import UIKit
import Pastel

class LaunchViewController: UIViewController {

    @IBOutlet weak var quoteTextView: UITextView!
    @IBOutlet weak var personTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let pastelView = PastelView(frame: view.bounds)
        
        // Custom Direction
        pastelView.startPastelPoint = .bottomLeft
        pastelView.endPastelPoint = .topRight
        
        // Custom Duration
        pastelView.animationDuration = 3.0
        
        // Custom Color
        
        pastelView.setColors([UIColor(red: 32/255, green: 76/255, blue: 255/255, alpha: 1.0),
                              UIColor(red: 32/255, green: 158/255, blue: 255/255, alpha: 1.0),
                              UIColor(red: 90/255, green: 120/255, blue: 127/255, alpha: 1.0),
                              UIColor(red: 58/255, green: 255/255, blue: 217/255, alpha: 1.0)])

        
        pastelView.startAnimation()
        view.insertSubview(pastelView, at: 0)
        
        quoteTextView.text = quote_list[0].quote
        personTextView.text = quote_list[0].person
    }
 
    @IBAction func tapToContinue(_ sender: Any) {
        self.performSegue(withIdentifier: "toTabBar", sender: self)
    }
    
}
