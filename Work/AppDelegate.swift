//
//  AppDelegate.swift
//  Work
//
//  Created by Paul Tang on 2019-04-02.
//  Copyright © 2019 Paul Tang. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Add quote to quote_list
        quote_list.append(Quote(quote: "Good, better, best. Never let it rest. 'Til your good is better and your better is best.", person: "St. Jerome"))
        
//        // Add note to quote_list
//        var note_1: Note = Note(content: "do what at what on what date", date: "2019-04-12")
//        var note_2: Note = Note(content: "go to volunteering fair at xxx, bring 50 dollars", date: "2019-04-15")
//        note_list.append(note_1)
//        note_list.append(note_2)
        
        if let savedNotes = loadNotes() {
            note_list = savedNotes
        }
        
        // Add weather to quote_list
        let sunny = UIImage(named: "sunny") ?? UIImage()
        let rainy = UIImage(named: "rainy") ?? UIImage()
        weather_list.append(Weather(temperature: "22°C", date: "Sun", image: sunny, weatherDescription: "Sunny"))
        weather_list.append(Weather(temperature: "23°C", date: "Mon", image: sunny, weatherDescription: "Sunny"))
        weather_list.append(Weather(temperature: "19°C", date: "Tue", image: rainy, weatherDescription: "Rainy"))
        weather_list.append(Weather(temperature: "20°C", date: "Wed", image: rainy, weatherDescription: "Rainy"))
        weather_list.append(Weather(temperature: "22°C", date: "Thu", image: sunny, weatherDescription: "Sunny"))
        weather_list.append(Weather(temperature: "17°C", date: "Fri", image: rainy, weatherDescription: "Rainy"))
        weather_list.append(Weather(temperature: "19°C", date: "Sat", image: sunny, weatherDescription: "Sunny"))

        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

